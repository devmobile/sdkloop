# com.tenth-avenue.loop

[![CI Status](https://img.shields.io/travis/Prigent-HighConnexion/com.tenth-avenue.loop.svg?style=flat)](https://travis-ci.org/Prigent-HighConnexion/com.tenth-avenue.loop)
[![Version](https://img.shields.io/cocoapods/v/com.tenth-avenue.loop.svg?style=flat)](https://cocoapods.org/pods/com.tenth-avenue.loop)
[![License](https://img.shields.io/cocoapods/l/com.tenth-avenue.loop.svg?style=flat)](https://cocoapods.org/pods/com.tenth-avenue.loop)
[![Platform](https://img.shields.io/cocoapods/p/com.tenth-avenue.loop.svg?style=flat)](https://cocoapods.org/pods/com.tenth-avenue.loop)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

com.tenth-avenue.loop is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'com.tenth-avenue.loop'
```

## Author

Prigent-HighConnexion, xtra.creativity@gmail.com

## License

com.tenth-avenue.loop is available under the MIT license. See the LICENSE file for more info.
