#
# Be sure to run `pod lib lint com.tenth-avenue.loop.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'com.tenth-avenue.loop'
  s.version          = '2.6.0'
  s.summary          = 'Integrating the « Tenthavenue France » Loop SDK is the first step towards improving revenue on google ads.'

  s.description      = <<-DESC
Beta version, Integrating the « Tenthavenue France » “Loop” SDK is the first step towards improving revenue on google ads with coupon. Doc : https://sites.google.com/view/sdkloop/accueil
                       DESC

  s.homepage         = 'https://bitbucket.org/devmobile/sdkloop'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Prigent-HighConnexion' => 'xtra.creativity@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/devmobile/sdkloop.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.ios.vendored_frameworks = '*.framework'

end
