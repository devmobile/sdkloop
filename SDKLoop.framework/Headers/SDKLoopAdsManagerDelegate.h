//
//  SDKLoopAdsManagerDelegate.h
//  SDKLoop
//
//  Created by Prigent ROUDAUT on 06/12/2017.
//  Copyright © 2017 HighConnexion. All rights reserved.
//

@import Foundation;
@import UIKit;

@protocol SDKLoopAdsManagerDelegate <NSObject>

@required
- (UIViewController*) viewControllerForAddPKPass;
@optional
- (void) didFinishAddPKPass;

@end
