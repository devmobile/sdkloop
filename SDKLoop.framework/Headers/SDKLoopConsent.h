//
//  SDKLoopConsent.h
//  SDKLoop
//
//  Created by Prigent ROUDAUT on 15/11/2017.
//  Copyright © 2017 HighConnexion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDKLoopConsent : NSObject

- (id) initWithDictionary:(NSDictionary*) dictionay;

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, strong) NSString * buttonNo;
@property (nonatomic, strong) NSString * buttonYes;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSNumber * date;
@property (nonatomic, strong) NSNumber * status;
@end
