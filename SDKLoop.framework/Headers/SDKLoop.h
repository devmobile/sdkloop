//
//  SDKLoop.h
//  SDKLoop
//
//  Created by Prigent ROUDAUT on 15/11/2017.
//  Copyright © 2017 HighConnexion. All rights reserved.
//

#import <SDKLoop/SDKLoopAdsManagerDelegate.h>
#import <SDKLoop/SDKLoopConsentManagerDelegate.h>
#import <SDKLoop/SDKLoopConsent.h>

//! Project version number for SDKLoop.
FOUNDATION_EXPORT double SDKLoopVersionNumber;

//! Project version string for SDKLoop
FOUNDATION_EXPORT const unsigned char SDKLoopVersionString[];

typedef enum SDKLoopConsentStatusTypes
{
    SDKLOOP_CONSENT_DENIED=0,
    SDKLOOP_CONSENT_ACCEPTED,
    SDKLOOP_CONSENT_UNKNOWN,
} SDKLoopConsentStatus;

// In this header, you should import all the public headers of your framework using statements like #import <SDKLoop/PublicHeader.h>

@interface  SDKLoop : NSObject

//FUNCTION FOR CONFIGURATION
+ (void) configureWithFileName:(NSString*) fileName; //file name without extension

//FUNCTION FOR TRACKING MANAGEMENT
+ (void) startTracking;

//FUNCTION FOR CONSENT MANANGEMENT
+ (void) verifyConsent:(NSString*) type withDelegate:(id<SDKLoopConsentManagerDelegate>) delegate withBlock:(void (^)(SDKLoopConsentStatus status)) onCompletion;
+ (void) getConsents:(void (^)(NSArray<SDKLoopConsent*> * data))onCompletion;
+ (void) getConsent:(NSString*) type onCompletion:(void (^)(SDKLoopConsent * consent))onCompletion;
+ (void) acceptConsent:(NSString*) type withStatus:(NSNumber*) status;

//FUNCTION FOR ADS MANANGEMENT
+ (void) manageReceiveMessage:(NSString*) message delegate:(id<SDKLoopAdsManagerDelegate>) delegate; //SMART INTERN DIALOG
+ (void) manageAppEvent:(NSString*) name withInfo:(NSString*) info delegate:(id<SDKLoopAdsManagerDelegate>) delegate; //DFP INTERN DIALOG
+ (void) getAdData:(void (^)(NSDictionary * data))onCompletion;

//FUNCTION FOR UPDATE SDK IN BACKGROUND
+ (void)fetchInBackground:(void (^)(UIBackgroundFetchResult))completionHandler;

@end
