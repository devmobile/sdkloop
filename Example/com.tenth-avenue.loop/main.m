//
//  main.m
//  com.tenth-avenue.loop
//
//  Created by Prigent-HighConnexion on 04/27/2018.
//  Copyright (c) 2018 Prigent-HighConnexion. All rights reserved.
//

@import UIKit;
#import "SDKLoopAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SDKLoopAppDelegate class]));
    }
}
