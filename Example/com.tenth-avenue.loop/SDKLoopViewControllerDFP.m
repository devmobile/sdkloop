//
//  SDKLoopViewControllerDFP.m
//  com.tenth-avenue.loop
//
//  Created by Prigent-HighConnexion on 04/27/2018.
//  Copyright (c) 2018 Prigent-HighConnexion. All rights reserved.
//

#import "SDKLoopViewControllerDFP.h"


@implementation SDKLoopViewControllerDFP

- (void)viewDidLoad
{
    [super viewDidLoad];

    [SDKLoop verifyConsent:@"install_customize_by_geo" withDelegate:self withBlock:^(SDKLoopConsentStatus status) {
        [SDKLoop startTracking];
    }];
    
    self.mBannerView.adSize = kGADAdSizeMediumRectangle;
    self.mBannerView.adUnitID = @"/21672687754/bloc_ad";
    self.mBannerView.rootViewController = self;
    
    [SDKLoop getAdData:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            DFPRequest * request = [DFPRequest request];
            request.customTargeting = data;
            [self.mBannerView loadRequest:request];
        });
    }];
}



- (void)adView:(GADBannerView *)banner didReceiveAppEvent:(NSString *)name withInfo:(NSString *GAD_NULLABLE_TYPE)info {
    [SDKLoop manageAppEvent:name withInfo:info delegate:self];
}

- (UIViewController*) viewControllerForAddPKPass{
    return self;
}

- (UIViewController*) viewControllerForPopUp{
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
