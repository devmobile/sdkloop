//
//  SDKLoopViewControllerSmart.h
//  com.tenth-avenue.loop
//
//  Created by Prigent-HighConnexion on 04/27/2018.
//  Copyright (c) 2018 Prigent-HighConnexion. All rights reserved.
//

@import UIKit;
@import SDKLoop;
#import <SmartAdServer-DisplaySDK/SASBannerView.h>

@interface SDKLoopViewControllerSmart : UIViewController<SDKLoopConsentManagerDelegate,SDKLoopAdsManagerDelegate, SASAdViewDelegate> {
    
}

@property (strong, nonatomic) SASBannerView *banner;

@end

