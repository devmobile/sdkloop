//
//  SDKLoopViewControllerSmart.m
//  com.tenth-avenue.loop
//
//  Created by Prigent-HighConnexion on 04/27/2018.
//  Copyright (c) 2018 Prigent-HighConnexion. All rights reserved.
//

#import "SDKLoopViewControllerSmart.h"

@implementation SDKLoopViewControllerSmart

- (void)viewDidLoad
{
    [super viewDidLoad];

    [SDKLoop verifyConsent:@"install_customize_by_geo" withDelegate:self withBlock:^(SDKLoopConsentStatus status) {
        [SDKLoop startTracking];
    }];
    [self createBanner];
/*
    [SDKLoop getAdData:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            DFPRequest * request = [DFPRequest request];
            request.customTargeting = data;
            [self.mBannerView loadRequest:request];
        });
    }];*/
}
- (void)createBanner {
    //_banner is an instance variable with default lifetime qualifier, which means '__strong':
    //the banner will be retained by the controller until it is released.
    _banner = [[SASBannerView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-300)/2, 80, 300, 250) loader:SASLoaderActivityIndicatorStyleWhite];
    _banner.backgroundColor = [UIColor blackColor];
    _banner.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _banner.delegate = self;
    _banner.accessibilityLabel = @"banner";
    _banner.modalParentViewController = self;
    [_banner loadFormatId:67586 pageId:@"949803" master:YES target:nil];
    
    [self.view addSubview:_banner];
}

- (void)adView:(nonnull SASAdView *)adView didReceiveMessage:(nonnull NSString *)message {
    [SDKLoop manageReceiveMessage:message delegate:self];
}


- (void)adView:(nonnull SASAdView *)adView didFailToLoadWithError:(nonnull NSError *)error {
    NSLog(@"didFailToLoadWithError error : %@", error);
}

- (void)adViewDidLoad:(nonnull SASAdView *)adView {
    NSLog(@"adViewDidLoad");
}

- (void)adView:(nonnull SASAdView *)adView didFailToPrefetchWithError:(nonnull NSError *)error {
    NSLog(@"didFailToPrefetchWithError %@",error);
}

//Called when the SASAdView instance failed to download the ad
- (void)adViewDidFailToLoad:(SASAdView *)adView error:(NSError *)error {
    NSLog(@"adViewDidFailToLoad %@", error);
}

- (UIViewController*) viewControllerForAddPKPass{
    return self;
}

- (UIViewController*) viewControllerForPopUp{
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
