//
//  SDKLoopAppDelegate.m
//  com.tenth-avenue.loop
//
//  Created by Prigent-HighConnexion on 04/27/2018.
//  Copyright (c) 2018 Prigent-HighConnexion. All rights reserved.
//



@import SDKLoop;
#import <SmartAdServer-DisplaySDK/SASAdView.h>
#import "SDKLoopAppDelegate.h"

#define kSiteID 242914
#define kBaseURL @"https://mobile.smartadserver.com"

@implementation SDKLoopAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [SDKLoop configureWithFileName:@"voici_voici_2"];

    [SASAdView setSiteID:kSiteID baseURL:kBaseURL];
    [SASAdView setLoggingEnabled:YES];
    // Override point for customization after application launch.
    return YES;
}


-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    [SDKLoop fetchInBackground:^(UIBackgroundFetchResult result) {
        completionHandler(result);
    }];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    /* Activate LivePreview for Smart AdServer SDK */
    /* Add the corresponding URL Type in your Info.plist */
    /* See documentation for more information about this feature */
    
    NSString *sasSchemeWithSiteID = [NSString stringWithFormat:@"sas%i", kSiteID];
    
    if ([[url scheme] isEqualToString:sasSchemeWithSiteID]) {
        return [SASAdView handleOpenURL:url];
    }
    
    
    
    //Add your own application:handleOpenURL: logic here...
    
    
    return NO;
}

@end
