//
//  SDKLoopAppDelegate.h
//  com.tenth-avenue.loop
//
//  Created by Prigent-HighConnexion on 04/27/2018.
//  Copyright (c) 2018 Prigent-HighConnexion. All rights reserved.
//

@import UIKit;

@interface SDKLoopAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
